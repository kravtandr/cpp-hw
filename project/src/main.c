//Вариант #61
//Составить программу поиска значения, наиболее часто встречающегося в заданной матрице. 
//Вычисления оформить в виде функции, принимающей на вход матрицу, заданную в виде вектора векторов, 
//и ее размеры по каждому измерению. На выход функция должна возвращать наиболее часто встречаемое значение.

#define _CRT_SECURE_NO_WARNINGS
/* Размер буфера */
#define CLI_BUFFER_SIZE 20

#include <stdio.h>
#include <malloc.h>
#include "hash_table.h"
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <string.h>


typedef struct
{
	int** v;
	int rows;
	int cols;
}matrix;

int solution(matrix m);
int** create_matrix(matrix m);
void print_matrix(matrix m);
void del_matrix(matrix m);
int find_max_count(matrix m);
int cli_read_int(int* value, const char* prompt);



int main(void)
{
	system("chcp 1251");
	system("cls");
	int status = 0;
	int rows = 0;
	int cols = 0;
	matrix m;
	do {
		status = cli_read_int(&rows, "Введите целое число строк матрицы");
		if (status != 0) {
			printf("Попробуйте еще раз.\n");
		}
	} while (status != 0);
	do {
		status = cli_read_int(&cols, "Введите целое число столбцов матрицы");
		if (status != 0) {
			printf("Попробуйте еще раз.\n");
		}
	} while (status != 0);
	m.rows = rows;
	m.cols = cols;
	//printf("Вы ввели: %3d%3d\n", rows, cols);
	printf("Наиболее часто встречаемое значение: %2d", solution(m));
	system("pause");
	return 0;
}
int solution(matrix m) {
	int res;
	m.v = create_matrix(m);
	//m.v = create_matrix_test(3, 3);
	print_matrix(m);
	res = find_max_count(m);
	del_matrix(m);
	return res;
}

int cli_read_int(int* value, const char* prompt)
{
	size_t length = 0;
	char* end = NULL;
	char buf[CLI_BUFFER_SIZE] = "";

	/* Проверка параметров */
	assert(value);
	assert(prompt);

	/* Приглашение */
	printf("%s: ", prompt);
	fflush(stdout);

	/* Чтение в буфер */
	if (!fgets(buf, sizeof(buf), stdin)) {
		return 1;
	}

	/* Удаление символа перевода строки */
	length = strlen(buf);
	if (buf[length - 1] == '\n') {
		buf[--length] = '\0';

		/* Перевод из строки в число */
		errno = 0;
		*value = strtol(buf, &end, 10);

		/* Обработка ошибок */
		if (length == 0) {
			fprintf(stderr, "Ошибка: введена пустая строка.\n");
			return 1;
		}
		if (errno != 0 || *end != '\0') {
			fprintf(stderr, "Ошибка: некорректный символ.\n");
			fprintf(stderr, "\t%s\n", buf);
			fprintf(stderr, "\t%*c\n", (int)(end - buf) + 1, '^');
			return 1;
		}
	}
	else {
		/* Строка прочитана не полностью
		   Пропустить остаток строки      */
		scanf("%*[^\n]");
		scanf("%*c");
		fprintf(stderr, "Ошибка: не вводите больше чем %d символа(ов).\n", CLI_BUFFER_SIZE - 2);

		return 1;
	}

	return 0;
}

int** create_matrix(matrix m) {
	//printf("%6d", m.rows);
	//printf("%6d", m.cols);
	int** v = (int**)malloc(m.rows * sizeof(int*));
	for (int i = 0; i < m.rows; i++)
		// NB: в каждой строке значение M может быть разным
		v[i] = (int*)malloc(m.cols * sizeof(int));

	printf("Заполнение матрицы:\n");
	int data = 0;
	for (int i = 0; i < m.rows; i++) {
		for (int j = 0; j < m.cols; j++) {
			//printf("%6d", v[i][j]);
			int status = 0;
			matrix m;
			do {
				status = cli_read_int(&data, "Введите целое число");
				if (status != 0) {
					printf("Попробуйте еще раз.\n");
				}
			} while (status != 0);
			v[i][j] = data;
		}
		//data = 1;
	}
	//print_matrix(v);
	return v;
}


void print_matrix(matrix m) {
	for (int i = 0; i < m.rows; i++) {
		for (int j = 0; j < m.cols; j++)
			printf("%6d", m.v[i][j]);
		printf("\n");
	}
}

void del_matrix(matrix m) {
	// удаление вектора векторов
	for (int i = 0; i < m.rows; i++)
		free(m.v[i]);
	free(m.v);
	//delete m;
}

int find_max_count(matrix m) {
	printf("=====================HASH-TABLE====================\n");
	uintptr_t v2;
	table_t  tb;
	void* k;
	iter_t it;
	slist* p;
	char* w, b[32];
	table_init(&tb, &cmp_int, &hash_int, NULL);
	
	long val = 0;
	int count = 0;
    void* val_insert;
	for (int i = 0; i < m.rows; i++) {
		for (int j = 0; j < m.cols; j++) {
			printf("%6d\n", m.v[i][j]);
			val = m.v[i][j];
			p = table_insert(&tb, (int*)val, 0);
			if (p != NULL) {
				++p->val;
			}

		}
	}
	
	//вывести 
	int max_count = 0;
	int value = 0;
	iter_reset(&it);
    void* v2_val;
	while (iter_each(&it, &tb, &k, &v2)) {
        v2_val=(int*)v2;
		printf("%3d (%u)\n", (long)k, *((unsigned int*)(&v2_val)));
		if ((unsigned int)v2 > max_count) {
			max_count = (unsigned int)v2;
			value = (long)k;
		}
	}
	table_clear(&tb);
	printf("===================================================\n\n");

	printf("Result: %2d (%u)\n", value, max_count);
	return value;

}
