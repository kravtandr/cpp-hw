#include "gtest/gtest.h"

extern "C" {
    #include "../src/hash_table.h"
    #include "../src/source.c"
}

TEST(Matrix, find_max_count1) {
  matrix m;
  m.rows=2;
  m.cols=2;
  int** v = (int**)malloc(m.rows * sizeof(int*));
	for (int i = 0; i < m.rows; i++)
		v[i] = (int*)malloc(m.cols * sizeof(int));
    m.v=v;
  m.v[0][0]=1;
  m.v[0][1]=1;
  m.v[1][0]=1;
  m.v[1][1]=2;
  ASSERT_EQ(find_max_count(m), 1);
  for (int i = 0; i < m.rows; i++)
	 	free(m.v[i]);
	free(m.v);
  //ASSERT_EQ(1, 1);

}

TEST(Matrix, find_max_count2) {
  matrix m;
  m.rows=2;
  m.cols=2;
  int** v = (int**)malloc(m.rows * sizeof(int*));
	for (int i = 0; i < m.rows; i++)
		v[i] = (int*)malloc(m.cols * sizeof(int));
    m.v=v;
  m.v[0][0]=1;
  m.v[0][1]=2;
  m.v[1][0]=2;
  m.v[1][1]=4;
  ASSERT_EQ(find_max_count(m), 2);
  for (int i = 0; i < m.rows; i++)
		free(m.v[i]);
	free(m.v);
}
TEST(Matrix, find_max_count3) {
  matrix m;
  m.rows=1;
  m.cols=1;
  int** v = (int**)malloc(m.rows * sizeof(int*));
	for (int i = 0; i < m.rows; i++)
		v[i] = (int*)malloc(m.cols * sizeof(int));
    m.v=v;
  m.v[0][0]=1;


  ASSERT_EQ(find_max_count(m), 1);
  for (int i = 0; i < m.rows; i++)
		free(m.v[i]);
	free(m.v);

}


// TEST(matrix, solution) {

//   ASSERT_EQ(solution(m), 1);

// }



int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
